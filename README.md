
<h1> README </h1>

Its a sample health insurance premium calculator.

src/TestClass.java - Java test class to test the app using console

HealthInsurancePremiumCalculator [emids_test/src/org/emids/biz]- Main Controller for calculating the interest

HealthInsurancePremiumCalculatorTest [emids_test/test/org/emids/biz]- sample 2 short Test cases written using Junit4

Person[emids_test/src/org/emids/model] - Model class for Person who wants to query for premium.

HealthConstants & Habbits [emids_test/src/org/emids/model] - 2 Enums for Person Habbits and Health Conditions
