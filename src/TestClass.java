import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.emids.biz.HealthInsurancePremiumCalculator;
import org.emids.model.Habbits;
import org.emids.model.HealthConstants;
import org.emids.model.Person;


public class TestClass {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Name : ");
		String name = scanner.next();
		System.out.println();
		
		System.out.print("Gender [M or F or O] : ");
		char gender = scanner.next().charAt(0);
		System.out.println();
		
		System.out.print("Age : ");
		int age = scanner.nextInt();
		System.out.println();
		
		List<HealthConstants> healthConstants = new ArrayList<>();
		
		System.out.print("Health Conditions (enter Y/N) : "
				+ "\n\t\t Hypertension : ");
		char ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			healthConstants.add(HealthConstants.HYPER_TENSION);
		}
		
		System.out.print("\t\t Blook pressure : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			healthConstants.add(HealthConstants.BLOOD_PRESSURE);
		}
		
		System.out.print("\t\t Blood sugar : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			healthConstants.add(HealthConstants.BLOOD_SUGAR);
		}
		
		System.out.print("\t\t Overweight : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			healthConstants.add(HealthConstants.OVER_WEIGHT);
		}

		
		List<Habbits> habbits = new ArrayList<>();
		
		System.out.print("Habbits (enter Y/N) : "
				+ "\n\t\t ALCHOHAL : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			habbits.add(Habbits.ALCHOHAL);
		}
		
		System.out.print("\t\t DAILY_EXCERCISE : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			habbits.add(Habbits.DAILY_EXCERCISE);
		}
		
		System.out.print("\t\t DRUGS : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			habbits.add(Habbits.DRUGS);
		}
		
		System.out.print("\t\t SMOKING : ");
		ans = scanner.next().charAt(0);
		if (ans == 'Y' || ans == 'y') {
			habbits.add(Habbits.SMOKING);
		}
		
		Person person = new Person(name, gender, age);
		person.setHabbits(habbits);
		person.setHealthConstants(healthConstants);
		
		String resultStr = "Health Insurance Premium for "+name+ " : Rs. ";
		if (gender == 'M' || gender == 'm')
			resultStr = "Health Insurance Premium for Mr. "+name+ " : Rs. ";
		if (gender == 'F' || gender == 'f')
			resultStr = "Health Insurance Premium for Miss/Mrs. "+name+ " : Rs. ";
		System.out.println("====================================");
		System.out.print(resultStr);
		System.out.println(new HealthInsurancePremiumCalculator().calculateInsurancePremium(person));
		System.out.println("====================================");
	}
}
