package org.emids.biz;

import org.emids.model.Habbits;
import org.emids.model.HealthConstants;
import org.emids.model.Person;

public class HealthInsurancePremiumCalculator {
	private static double basePremium;
	static {
		basePremium = 5000;
	}

	public static long calculateInsurancePremium(Person person) {
		if (person.getAge() <= 18) {
			return Math.round(basePremium);
		}
		calculatePremiumOnAge(person);
		calculatePremiumOnGender(person);
		calculatePremiumOnHealthConditions(person);
		calculatePremiumOnHabbits(person);
		return Math.round(basePremium);
	}

	protected static void calculatePremiumOnHabbits(Person person) {
		double premiumAccumulatedOnHabbits = 0;
		for (Habbits habbit : person.getHabbits()) {
			if (habbit == Habbits.DAILY_EXCERCISE) {
				premiumAccumulatedOnHabbits = ((basePremium * 3) / 100);
			} else {
				premiumAccumulatedOnHabbits = ((basePremium * 3) / 100);
			}
		}
	}

	protected static void calculatePremiumOnHealthConditions(Person person) {
		for (HealthConstants healthConstant : person.getHealthConstants()) {
			basePremium += ((basePremium * 1) / 100); 
		}
	}

	protected static void calculatePremiumOnGender(Person person) {
		if (person.getGender() == 'M' || person.getGender() == 'm') {
			basePremium += ((basePremium * 2) / 100);
		}
	}

	protected static void calculatePremiumOnAge(Person person) {
		int[] ageSlabs = { 18, 25, 30, 35 };
		for (int ageSlab : ageSlabs) {
			if (person.getAge() > ageSlab) {
				basePremium += ((basePremium * 10) / 100);
			}
		}
		if (person.getAge() > 40) {
			basePremium += ((basePremium * 20) / 100);
		}
	}

/*	public static void main(String[] args) {
		Person person = new Person("Norman Gomes", 'M', 34);
		List<HealthConstants> healthConstants = new ArrayList<>();
		healthConstants.add(HealthConstants.OVER_WEIGHT);
		
		List<Habbits> habbits = new ArrayList<>();
		habbits.add(Habbits.ALCHOHAL);
		habbits.add(Habbits.DAILY_EXCERCISE);

		person.setHabbits(habbits);
		person.setHealthConstants(healthConstants);

		System.out.println(calculateInsurancePremium(person));
	}*/
}
