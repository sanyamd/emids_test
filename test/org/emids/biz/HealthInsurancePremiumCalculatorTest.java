package org.emids.biz;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.emids.model.Habbits;
import org.emids.model.HealthConstants;
import org.emids.model.Person;
import org.junit.Before;
import org.junit.Test;

public class HealthInsurancePremiumCalculatorTest {
	HealthInsurancePremiumCalculator healthInsurancePremiumCalculator;
	
	@Before
	public void setUp() {
		healthInsurancePremiumCalculator = new HealthInsurancePremiumCalculator();
	}
	
	@Test
	public void testCalculateInsurancePremium_BELOW_18() {
		// Setup
		Person person = new Person("Norman Gomes", 'M', 17);
		List<HealthConstants> healthConstants = new ArrayList<>();
		healthConstants.add(HealthConstants.OVER_WEIGHT);
		
		List<Habbits> habbits = new ArrayList<>();
		habbits.add(Habbits.ALCHOHAL);
		habbits.add(Habbits.DAILY_EXCERCISE);
		long result = 5000;

		person.setHabbits(habbits);
		person.setHealthConstants(healthConstants);
		
		// Method Call
		long premium = healthInsurancePremiumCalculator.calculateInsurancePremium(person);
		
		// Verification
		assertEquals(result, premium);
	}
	
	@Test
	public void testCalculateInsurancePremium_ABOVE_18() {
		// Setup
		Person person = new Person("Norman Gomes", 'M', 34);
		List<HealthConstants> healthConstants = new ArrayList<>();
		healthConstants.add(HealthConstants.OVER_WEIGHT);
		
		List<Habbits> habbits = new ArrayList<>();
		habbits.add(Habbits.ALCHOHAL);
		habbits.add(Habbits.DAILY_EXCERCISE);
		long result = 6856;

		person.setHabbits(habbits);
		person.setHealthConstants(healthConstants);
		
		// Method Call
		long premium = healthInsurancePremiumCalculator.calculateInsurancePremium(person);
		
		// Verification
		assertEquals(result, premium);
	}
}
